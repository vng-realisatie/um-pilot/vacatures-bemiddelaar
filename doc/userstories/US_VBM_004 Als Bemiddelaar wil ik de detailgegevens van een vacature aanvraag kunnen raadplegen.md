# Feature : US_VBM_004 Als Bemiddelaar wil ik de detailgegevens van een vacature aanvraag kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                 |
|--------|---------------|---------------------------|
| 0.10   | februari 2023 | Initiele opzet            |

**Als** Bemiddelaar  
**Wil ik** de detailgegevens van een vacature aanvraag kunnen raadplegen
**Zodat ik** deze vacature kan bemiddelen

### Functioneel
Dit is een ondersteunende User Story die beschrijft waaraan de API dient te voldoen.

Voor het bemiddelen van een vacature geschikt is het nodig de details van deze vacature ter beschikking te hebben

### Technische Documentatie

Endpoint : "/aanvraagvacature/detail/{oin}/{vraagId}/{vumId}"

In de implementatie worden de volgende response codes genoemd
"200", description = "OK"
"400", description = "Bad request"
"403", description = "not authorized"
"422", description = "Fout bij uitvoeren van zoekvraag"
"429", description = "Too Many Requests, limiet is overschreden voor deze uitvraag"
"500", description = "Internal Server Error"
"503", description = "Service Unavailable"


#### Postconditie
Na een bewerking is altijd de log bijgewerkt

### Acceptatiecriteria

*Feature: Opvragen details vacature aanvraag*  
**Gegeven** de Client is geauthoriseerd  
**En** er bestaat een aanvraag vacature met een gegeven id
**Wanneer** de details van een aanvraag vacature voor een OIN met het gegeven id wordt opgevraagd  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** is vacature aanvraag inclusief relevante vacatures opgenomen in de response
**En** het aantal beschikbare opvragingen verminderd met 1
**En** zijn de vacatures gemarkeerd als bevraagd

*Feature: Opvragen niet bestaande vacature aanvraag*  
**Gegeven** de Client is geauthoriseerd  
**En** er bestaat een aanvraag vacature met een gegeven id
**Wanneer** een aanvraag vacature voor een OIN met het gegeven id wordt opgevraagd  
**Dan** retourneert de applicatie een http status 400 (Ongeldige aanroep)  

*Feature: Te veel bevragingen op vacature aanvraag*  
**Gegeven** de Client is geauthoriseerd  
**En** er bestaat een aanvraag vacature met een gegeven id
**En** het aantal toegestane bevragingen van deze aanvraag is bereikt
**Wanneer** de details van een aanvraag vacature voor een OIN met het gegeven id wordt opgevraagd
**Dan** retourneert de applicatie een http status 429 (Too Many Requests)

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  
**En** zijn de vacatures niet opgenomen in de bron  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad request)  
**En** zijn de vacatures niet opgenomen in de bron
