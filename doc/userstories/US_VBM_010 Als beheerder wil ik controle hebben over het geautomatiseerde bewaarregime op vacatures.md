# Feature : US_VBM_010 Als beheerder wil ik controle hebben over het geautomatiseerde bewaarregime op vacatures

versie 0.10

_Versionering_

| versie | datum         | opmerking                 |
|--------|---------------|---------------------------|
| 0.10   | februari 2023 | Initiele opzet            |

*Feature:*
**Als** beheerder
**Wil ik** controle hebben over het geautomatiseerde bewaarregime van toepassing op vacatures
**Zodat** ik kan waarborgen dat de applicatie aan de Systeemeisenset Privacy by Design voldoet

*Achtergrond:*
**Gegeven** een beheerder met een e-mailadres
**En** twee vragen die 8 dagen geleden gesteld zijn
**En** dat vragen ouder dan 7 dagen geautomatiseerd verwijderd worden

*Scenario: Vragen succesvol verwijderd*
**Wanneer** al deze vragen vandaag succesvol verwijderd zijn
**Dan** bevatten de logs van de applicatie een regel met de datum van vandaag en “2 oude vragen met succes verwijderd”
**En** ontvangt de beheerder hiervan bericht via e-mail

*Scenario: Vraag niet verwijderd*
**Wanneer** een vraag niet verwijderd kon worden
**Dan** bevatten de logs van de applicatie de volgende regels met de datum van vandaag:
| 1 oude vraag met succes verwijderd |
| 1 oude vraag niet verwijderd |
**En** ontvangt de beheerder hiervan bericht via e-mail

*Scenario: Fout tijdens geautomatiseerde verwijdering*
**Wanneer** er een onbekende fout optreedt tijdens geautomatiseerde verwijdering van vragen
**Dan** bevatten de logs van de applicatie een regel met de datum van vandaag en “fout opgetreden tijdens het verwijderen van vragen. Onbekend aantal vragen verwijderd.”
**En** ontvangt de beheerder hiervan bericht via e-mail