package nl.vng.vacatures_bemiddelaar.scheduler;

import nl.vng.vacatures_bemiddelaar.service.GemeenteService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ScheduledTasksTest {

    @InjectMocks
    private ScheduledTasks scheduledTasks;

    @Mock
    private GemeenteService service;

    @Test
    void removeAllExpiredAanvraag() {
        scheduledTasks.removeAllExpiredAanvraag();
        Mockito.verify(service).removeAllExpiredAanvraag();
    }
}