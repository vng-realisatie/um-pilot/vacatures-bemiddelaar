package nl.vng.vacatures_bemiddelaar.validator;

import nl.vng.vacatures_bemiddelaar.entity.Gedragscompetentie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Indirectly, tests the OneOfIntValidator
 */
class OneOfIntValidatorTest {

    private Validator validator;

    @BeforeEach
    public void setUp() {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();

    }

    @Test
    void isValid() {
        Gedragscompetentie gedragscompetentie = new Gedragscompetentie();
        gedragscompetentie.setCodeBeheersingGedragscompetentie(1);

        validator.validate(gedragscompetentie);
        Set<ConstraintViolation<Gedragscompetentie>> validate = validator.validate(gedragscompetentie);
        assertTrue(validate.isEmpty());
    }

    @Test
    void isNotValid() {
        Gedragscompetentie gedragscompetentie = new Gedragscompetentie();
        gedragscompetentie.setCodeBeheersingGedragscompetentie(1111);

        validator.validate(gedragscompetentie);
        Set<ConstraintViolation<Gedragscompetentie>> validate = validator.validate(gedragscompetentie);
        assertFalse(validate.isEmpty());
    }
}