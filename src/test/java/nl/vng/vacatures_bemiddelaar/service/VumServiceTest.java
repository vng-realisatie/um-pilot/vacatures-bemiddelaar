package nl.vng.vacatures_bemiddelaar.service;

import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesCallbackRequest;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchingProfielen;
import nl.vng.vacatures_bemiddelaar.entity.AanvraagVacature;
import nl.vng.vacatures_bemiddelaar.entity.MPVacatureMatch;
import nl.vng.vacatures_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.vacatures_bemiddelaar.repository.AanvraagVacatureRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VumServiceTest {

    public static final String VRAAG_ID = "VRAAG_ID";
    @Mock
    private AanvraagVacatureRepository repository;

    @InjectMocks
    private VumService vumService;

    @Test
    void shouldHandleCallback() {

        VacatureMatchesCallbackRequest callbackRequest = new VacatureMatchesCallbackRequest();
        callbackRequest.setVraagID(VRAAG_ID);
        callbackRequest.setMatches(new VacatureMatchingProfielen());
        callbackRequest.getMatches().setMpVacatureMatches(Arrays.asList(new MPVacatureMatch()));
        Optional<AanvraagVacature> aanvraagInDbOpt = Optional.of(new AanvraagVacature());
        when(repository.findById(callbackRequest.getVraagID())).thenReturn(aanvraagInDbOpt);

        vumService.handleCallback(callbackRequest);

        verify(repository).findById(callbackRequest.getVraagID());
        verify(repository).save(any());

    }

    @Test
    void shouldHandleNotKnownVraag() {

        VacatureMatchesCallbackRequest callbackRequest = new VacatureMatchesCallbackRequest();
        callbackRequest.setVraagID(VRAAG_ID);
        callbackRequest.setMatches(new VacatureMatchingProfielen());
        callbackRequest.getMatches().setMpVacatureMatches(Arrays.asList(new MPVacatureMatch()));
        Optional<AanvraagVacature> aanvraagInDbOpt = Optional.empty();
        when(repository.findById(callbackRequest.getVraagID())).thenReturn(aanvraagInDbOpt);

        Assertions.assertThrows(VraagIdNotFoundException.class, () -> vumService.handleCallback(callbackRequest), "400 BAD_REQUEST \"Vraag ID niet gevonden\"");
    }
}