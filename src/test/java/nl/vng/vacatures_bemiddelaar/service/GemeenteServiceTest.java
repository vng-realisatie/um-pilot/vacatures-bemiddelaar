package nl.vng.vacatures_bemiddelaar.service;

import nl.vng.vacatures_bemiddelaar.config.ConfigProperties;
import nl.vng.vacatures_bemiddelaar.dto.InlineResponse200;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequestGemeente;
import nl.vng.vacatures_bemiddelaar.entity.AanvraagVacature;
import nl.vng.vacatures_bemiddelaar.exception.UnprocessableException;
import nl.vng.vacatures_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.vacatures_bemiddelaar.mapper.SimpleMapper;
import nl.vng.vacatures_bemiddelaar.repository.AanvraagVacatureRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GemeenteServiceTest {

    public static final String VRAAG_ID = "VraagId";
    public static final String VUM_ID = "VUM_ID";
    @Mock
    private AanvraagVacatureRepository repository;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private VacatureMatchesRequest request;
    @Mock
    private SimpleMapper mapper;
    @Mock
    private ConfigProperties properties;

    @InjectMocks
    private GemeenteService gemeenteService;

    @Test
    void shouldFindAll() {

        gemeenteService.findAll(properties.getVumProviderOin());

        verify(repository).findByOin(properties.getVumProviderOin());
    }

    @Test
    void shouldFindByVraagIdAndOin() {

        when(repository.findByVraagIdAndOin(VRAAG_ID, properties.getVumProviderOin())).thenReturn(new AanvraagVacature());

        gemeenteService.findByVraagIdAndOin(VRAAG_ID, properties.getVumProviderOin());

        verify(repository).findByVraagIdAndOin(VRAAG_ID, properties.getVumProviderOin());

    }

    @Test
    void shouldThrowVraagIdNotFoundException() {

        Assertions.assertThrows(VraagIdNotFoundException.class, () -> gemeenteService.findByVraagIdAndOin(VRAAG_ID, properties.getVumProviderOin()), "VraagIdNotFoundException is verwacht");
    }

    @Test
    void shouldMakeRequestMatchesVum() {

        VacatureMatchesRequestGemeente matchesRequestGemeente = new VacatureMatchesRequestGemeente();

        when(properties.getVumUrlMatches()).thenReturn("url");
        when(mapper.vacatureMatchesRequestGemeenteToVacatureMatchesRequest(matchesRequestGemeente)).thenReturn(new VacatureMatchesRequest());
        when(restTemplate.postForObject(eq("url"), any(HttpEntity.class), eq(InlineResponse200.class))).thenReturn(new InlineResponse200());
        when(repository.existsById(any())).thenReturn(false);

        gemeenteService.makeRequestMatchesVum(matchesRequestGemeente, properties.getVumProviderOin());

        verify(properties).getCallbackUrl();
        verify(properties).getVumUrlMatches();

        verify(restTemplate).postForObject(eq("url"), any(HttpEntity.class), eq(InlineResponse200.class));
        verify(repository).existsById(any());
        verify(repository).save(any());
    }

    @Test
    void shouldNotMakeRequestMatchesVum() {

        VacatureMatchesRequestGemeente matchesRequestGemeente = new VacatureMatchesRequestGemeente();

        when(properties.getVumUrlMatches()).thenReturn("url");
        when(mapper.vacatureMatchesRequestGemeenteToVacatureMatchesRequest(matchesRequestGemeente)).thenReturn(new VacatureMatchesRequest());
        when(restTemplate.postForObject(eq("url"), any(HttpEntity.class), eq(InlineResponse200.class))).thenReturn(new InlineResponse200());
        when(repository.existsById(any())).thenReturn(true);

        assertThrows(UnprocessableException.class, () -> gemeenteService.makeRequestMatchesVum(matchesRequestGemeente, properties.getVumProviderOin()), "422 UNPROCESSABLE_ENTITY \"Vraag id bestaat al in database\"");
    }

    @Test
    void shouldHandleRestClientException() {

        VacatureMatchesRequestGemeente matchesRequestGemeente = new VacatureMatchesRequestGemeente();

        when(properties.getVumUrlMatches()).thenReturn("url");
        when(mapper.vacatureMatchesRequestGemeenteToVacatureMatchesRequest(matchesRequestGemeente)).thenReturn(new VacatureMatchesRequest());
        when(restTemplate.postForObject(eq("url"), any(HttpEntity.class), eq(InlineResponse200.class))).thenThrow(new RestClientException(""));

        assertThrows(UnprocessableException.class, () -> gemeenteService.makeRequestMatchesVum(matchesRequestGemeente, properties.getVumProviderOin()), "422 UNPROCESSABLE_ENTITY \"Rest client error\"");
    }

    @Test
    void shouldHandleNPE() {

        VacatureMatchesRequestGemeente matchesRequestGemeente = new VacatureMatchesRequestGemeente();

        when(properties.getVumUrlMatches()).thenReturn("url");
        when(mapper.vacatureMatchesRequestGemeenteToVacatureMatchesRequest(matchesRequestGemeente)).thenReturn(new VacatureMatchesRequest());
        when(restTemplate.postForObject(eq("url"), any(HttpEntity.class), eq(InlineResponse200.class))).thenThrow(new NullPointerException(""));

        assertThrows(UnprocessableException.class, () -> gemeenteService.makeRequestMatchesVum(matchesRequestGemeente, properties.getVumProviderOin()), "422 UNPROCESSABLE_ENTITY \"Rest client error\"");
    }

    @Test
    void shouldRemoveAllExpiredAanvraag() {

        when(properties.getDaysToExpiry()).thenReturn(0);
        when(repository.findAllByCreatieDatumBefore(any())).thenReturn(new ArrayList<>());

        gemeenteService.removeAllExpiredAanvraag();

        verify(repository).deleteAll(anyList());

    }
}