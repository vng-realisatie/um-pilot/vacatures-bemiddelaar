package nl.vng.vacatures_bemiddelaar.controller;

import nl.vng.vacatures_bemiddelaar.dto.InlineResponse200;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequestGemeente;
import nl.vng.vacatures_bemiddelaar.entity.AanvraagVacature;
import nl.vng.vacatures_bemiddelaar.entity.Vacature;
import nl.vng.vacatures_bemiddelaar.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.vacatures_bemiddelaar.service.ElkService;
import nl.vng.vacatures_bemiddelaar.service.GemeenteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the GemeenteController
 */
@ExtendWith(MockitoExtension.class)
class GemeenteControllerTest {

    public static final String OIN = "OIN";
    public static final String VRAAG_ID = "VRAAG_ID";
    public static final String VUM_ID = "VUM_ID";
    @Mock
    private ElkService elkService;

    @Mock
    private GemeenteService gemeenteService;

    @Mock
    private SecurityContext securityContext;

    private JwtAuthenticationToken authentication;

    @InjectMocks
    private GemeenteController gemeenteController;

    @BeforeEach
    private void setUp() {
        authentication = new JwtAuthenticationToken(Jwt
                .withTokenValue("oin")
                .claim("oin", OIN)
                .header("", "")
                .build());
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void shouldCreateAanvraagVacatures() {

        VacatureMatchesRequestGemeente requestGemeente = mock(VacatureMatchesRequestGemeente. class);

        when(gemeenteService.makeRequestMatchesVum(requestGemeente, OIN)).thenReturn(new InlineResponse200());

        gemeenteController.createAanvraagVacature(requestGemeente, OIN);

        String requestSignature = "POST /aanvraagvacature/{oin}";
        String requestDescription = "Endpoint to post a request for vacatures matching aanvraagVacature";
        String requestPath = "/aanvraagvacature/OIN";

        verify(elkService).handleRequest(requestGemeente, OIN, OIN, requestSignature, requestDescription, requestPath);
        verify(gemeenteService).makeRequestMatchesVum(requestGemeente, OIN);
    }

    @Test
    void shouldFindAll() {

        List<AanvraagVacature> aanvraagVacatures = Arrays.asList(new AanvraagVacature());
        when(gemeenteService.findAll(OIN)).thenReturn(aanvraagVacatures);

        List<AanvraagVacature> all = gemeenteController.allAanvraagVacatures(OIN);

        String requestSignature = "GET /aanvraagvacature/lijst/{oin}";
        String requestDescription = "Endpoint to retrieve all outstanding aanvragen for the given OIN";
        String requestPath = "/aanvraagwerkzoekende/lijst/OIN";

        verify(gemeenteService).findAll(OIN);
        verify(elkService).handleRequest(null, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertEquals(aanvraagVacatures, all);
    }

    @Test()
    void shouldFailBuildingOinUserGetAll() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.allAanvraagVacatures("WRONG"), "WrongOinException is verwacht");
    }

    @Test()
    void shouldFailBuildingOinUserCreate() {

        VacatureMatchesRequestGemeente requestGemeente = mock(VacatureMatchesRequestGemeente. class);

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.createAanvraagVacature(requestGemeente, "WRONG"), "WrongOinException is verwacht");
    }

    @Test
    void shouldGetDetailVacature() {

        when(gemeenteService.makeRequestDetailVacature(VRAAG_ID, VUM_ID, OIN)).thenReturn(new Vacature());

        Vacature vacature = gemeenteController.getDetailVacature(OIN, VRAAG_ID, VUM_ID);

        String requestSignature = "GET /aanvraagvacature/detail/{oin}/{vraagId}/{vumId}";
        String requestDescription = "Endpoint to retrieve detailed vacature for given vumID";
        String requestPath = "/aanvraagvacature/detail/OIN/VRAAG_ID/VUM_ID";

        verify(gemeenteService).makeRequestDetailVacature(VRAAG_ID, VUM_ID, OIN);
        verify(elkService).handleRequest(vacature, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertNotNull(vacature);
    }

    @Test
    void shouldNotGetDetailVacature() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.getDetailVacature("WRONG", VRAAG_ID, VUM_ID), "WrongOinException is verwacht");

        gemeenteController.getDetailVacature(OIN, VRAAG_ID, VUM_ID);
    }

    @Test
    void shouldGetAanvraagVacature() {

        when(gemeenteService.findByVraagIdAndOin(VRAAG_ID, OIN)).thenReturn(new AanvraagVacature());

        AanvraagVacature aanvraagVacature = gemeenteController.getAanvraagVacature(OIN, VRAAG_ID);

        String requestSignature = "GET /aanvraagvacature/{oin}/{vraagId}";
        String requestDescription = "Endpoint to retrieve aanvraagvacature for the given OIN with vraag ID";
        String requestPath = "/aanvraagvacature/OIN/VRAAG_ID";

        verify(gemeenteService).findByVraagIdAndOin(VRAAG_ID, OIN);
        verify(elkService).handleRequest(null, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertNotNull(aanvraagVacature);
    }

    @Test
    void shouldNotGetAanvraagVacature() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.getAanvraagVacature("WRONG", VRAAG_ID), "WrongOinException is verwacht");

        gemeenteController.getAanvraagVacature(OIN, VRAAG_ID);
    }

}