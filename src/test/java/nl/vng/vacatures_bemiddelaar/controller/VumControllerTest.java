package nl.vng.vacatures_bemiddelaar.controller;

import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesCallbackRequest;
import nl.vng.vacatures_bemiddelaar.service.ElkService;
import nl.vng.vacatures_bemiddelaar.service.VumService;
import nl.vng.vacatures_bemiddelaar.config.ConfigProperties;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the VumController
 */
@ExtendWith(MockitoExtension.class)
class VumControllerTest {

    @Mock
    private VumService vumService;

    @Mock
    private ElkService elkService;
    
    @Mock
    private ConfigProperties properties;

    @InjectMocks
    private VumController vumController;

    private final String xVumVraagId = "xVumVraagId";
    private final String xVUMBerichtVersie = "xVUMBerichtVersie";
    private final String xVUMToParty = "xVUMToParty";
    private final String xVUMViaParty = "xVUMViaParty";
    private final String xVUMFromParty = "xVUMFromParty";
    private final String xForwardedFor = "01234567890123456789";

    @Mock
    private VacatureMatchesCallbackRequest matchesRequest;

    @Test
    void shouldHandleCallBack() {

        String requestSignature = "POST /aanvraagvacature/callback";
        String callback = "Callback methode om matching vacature binnen te krijgen voor Vraag ID";
        String requestPath = "/aanvraagvacature/callback";
        
        when(properties.getVumProviderOin()).thenReturn("00000001234567890123456789");
        ResponseEntity<VacatureMatchesCallbackRequest> response = vumController.callback(xVUMBerichtVersie, xVUMToParty, xVUMFromParty, xVumVraagId, xForwardedFor, matchesRequest, xVUMViaParty);

        verify(elkService).handleRequest(matchesRequest, xVUMToParty, xVUMFromParty, requestSignature, callback, requestPath);
        verify(vumService).handleCallback(matchesRequest);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
}