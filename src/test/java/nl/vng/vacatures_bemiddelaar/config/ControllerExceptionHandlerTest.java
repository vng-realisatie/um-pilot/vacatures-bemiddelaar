package nl.vng.vacatures_bemiddelaar.config;

import nl.vng.vacatures_bemiddelaar.exception.NoMoreAccessRequestsLeftException;
import nl.vng.vacatures_bemiddelaar.exception.UnprocessableException;
import nl.vng.vacatures_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.vacatures_bemiddelaar.exception.MismatchCertWithOinVumProviderException;
import nl.vng.vacatures_bemiddelaar.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.vacatures_bemiddelaar.util.Error;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.bind.MissingRequestHeaderException;

import javax.validation.ConstraintViolationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the ControllerExceptionHandler
 */
@ExtendWith(MockitoExtension.class)
class ControllerExceptionHandlerTest {

    public static final String MESSAGE = "message";
    private ControllerExceptionHandler exceptionHandler;

    @BeforeEach
    void setUp() {
        exceptionHandler = new ControllerExceptionHandler();
    }

    @Test
    void shouldHandleMissingRequestHeaderException() {
        MissingRequestHeaderException missingRequestHeaderException = mock(MissingRequestHeaderException.class);
        when(missingRequestHeaderException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleMissingRequestHeaderException(missingRequestHeaderException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONVOLLEDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.ONVOLLEDIGE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleMismatchRequestWithOinBemiddelaarException() {

        MismatchRequestWithOinBemiddelaarException wrongOinException = new MismatchRequestWithOinBemiddelaarException();
        Error error = exceptionHandler.handleMismatchRequestWithOinBemiddelaarException(wrongOinException);

        assertEquals("403 FORBIDDEN \"Invalide OIN\"", error.getDetails());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getRight(), error.getMessage());

    }
    
    @Test
    void shouldHandleMismatchCertWithOinVumProviderException() {

    	MismatchCertWithOinVumProviderException wrongOinException = new MismatchCertWithOinVumProviderException();
        Error error = exceptionHandler.handleMismatchCertWithOinVumProviderException(wrongOinException);

        assertEquals("403 FORBIDDEN \"Het OIN is niet gelijk aan het OIN van de VUM Voorziening (HTTPS Certificaat)\"", error.getDetails());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleNumberFormatException() {

        NoMoreAccessRequestsLeftException noMoreAccessRequestsLeftException = mock(NoMoreAccessRequestsLeftException.class);
        when(noMoreAccessRequestsLeftException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleNoMoreAccessRequestsLeftException(noMoreAccessRequestsLeftException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.TOO_MANY_REQUESTS.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.TOO_MANY_REQUESTS.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleVraagIdFoundException() {

        VraagIdNotFoundException vraagIdNotFoundException = mock(VraagIdNotFoundException.class);
        when(vraagIdNotFoundException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleVraagIdNotFoundException(vraagIdNotFoundException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.FOUT_BIJ_UITVOEREN_VAN_ZOEKVRAAG, error.getMessage());

    }

    @Test
    void shouldHandleConstraintViolationException() {

        ConstraintViolationException constraintViolationException = mock(ConstraintViolationException.class);
        when(constraintViolationException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleConstraintViolationException(constraintViolationException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleUnprocessableException() {

        UnprocessableException unprocessableException = new UnprocessableException("reason");
        Error error = exceptionHandler.handleUnprocessableException(unprocessableException);

        assertEquals("422 UNPROCESSABLE_ENTITY \"reason\"", error.getDetails());
        assertEquals(ControllerExceptionHandler.FOUT_BIJ_UITVOEREN.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.FOUT_BIJ_UITVOEREN.getRight(), error.getMessage());

    }
}