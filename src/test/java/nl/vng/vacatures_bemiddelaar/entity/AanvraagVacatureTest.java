package nl.vng.vacatures_bemiddelaar.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AanvraagVacatureTest {

    @Test
    void shouldAdd() {

        AanvraagVacature aanvraagVacature = new AanvraagVacature();
        assertTrue(aanvraagVacature.getVacatures().isEmpty());

        aanvraagVacature.addVacature(new MPVacatureMatch());

        assertEquals(aanvraagVacature.getVacatures().size(), 1);
    }

}