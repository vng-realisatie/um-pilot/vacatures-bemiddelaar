package nl.vng.vacatures_bemiddelaar.mapper;

import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequestGemeente;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@JsonTest
@ActiveProfiles("test")
class SimpleMapperITCase {

    private static final SimpleMapper mapper = new SimpleMapperImpl();

    private static VacatureMatchesRequestGemeente requestGemeente;

    @Autowired
    private ObjectMapper objectMapper;

    private static final String PATH = "./src/test/resources/";

    @BeforeEach
    void setUp() {
        try {
            // get the request as an object from the json file.
            requestGemeente = objectMapper.readValue(new File(PATH, "vacature-matches-request-gemeente.json"), VacatureMatchesRequestGemeente.class);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void vacatureMatchesRequestGemeenteToVacatureMatchesRequest() {
        VacatureMatchesRequest request = mapper.vacatureMatchesRequestGemeenteToVacatureMatchesRequest(requestGemeente);

        assertThat(request.getCallbackURL()).isNull();
//        assertThat(request.getPostcode()).isEqualTo(requestGemeente.getPostcode());
//        assertThat(request.getStraal()).isEqualTo(requestGemeente.getStraal());
        assertThat(request.getVraagObject()).isEqualTo(requestGemeente.getVraagObject());
    }

    @Test
    void vacatureMatchesRequestGemeenteNull() {
        requestGemeente = null;
        VacatureMatchesRequest request = mapper.vacatureMatchesRequestGemeenteToVacatureMatchesRequest(requestGemeente);
        assertThat(request).isNull();
    }
}