package nl.vng.vacatures_bemiddelaar.repository;

import nl.vng.vacatures_bemiddelaar.entity.AanvraagVacature;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface AanvraagVacatureRepository extends JpaRepository<AanvraagVacature, String> {

    List<AanvraagVacature> findAllByCreatieDatumBefore(LocalDate expiryDate);

    List<AanvraagVacature> findByOin(String oin);

    AanvraagVacature findByVraagIdAndOin(String vraagId, String oin);

}
