package nl.vng.vacatures_bemiddelaar.dto;

import nl.vng.vacatures_bemiddelaar.entity.MPVacature;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VacatureMatchesRequest {

    @NotNull
    private String callbackURL;

    @NotNull
    @Valid
    private MPVacature vraagObject;
}
