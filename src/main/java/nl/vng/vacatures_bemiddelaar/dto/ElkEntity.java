package nl.vng.vacatures_bemiddelaar.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "vacatures-bemiddelaar-index")
public class ElkEntity {

    @Id
    private String id;

    private Object request;

    @Field(type = FieldType.Date, format = {}, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'") //TODO: UTC TIMEZONE
    private LocalDateTime timestamp = LocalDateTime.now();

    private String toOin;

    private String fromOin;


    private String requestSignature;

    private String requestDescription;

    private String requestPath;

    public ElkEntity(
            final Object req,
            final String oinTo,
            final String oinFrom,
            final String signature,
            final String description,
            final String path
    ) {
        this.request = req;
        this.toOin = oinTo;
        this.fromOin = oinFrom;
        this.requestSignature = signature;
        this.requestDescription = description;
        this.requestPath = path;
    }
}
