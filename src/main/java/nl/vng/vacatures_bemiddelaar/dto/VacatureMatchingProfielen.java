package nl.vng.vacatures_bemiddelaar.dto;

import nl.vng.vacatures_bemiddelaar.entity.MPVacatureMatch;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * VacatureMatchingProfielen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VacatureMatchingProfielen {
    @JsonProperty("matches")
    @NotNull
    private List<MPVacatureMatch> mpVacatureMatches;

    @Override
    public String toString() {
        return "VacatureMatchingProfielen{" +
                "mpVacatureMatches=" + mpVacatureMatches +
                '}';
    }
}
