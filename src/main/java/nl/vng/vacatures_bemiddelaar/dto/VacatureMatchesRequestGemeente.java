package nl.vng.vacatures_bemiddelaar.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.vng.vacatures_bemiddelaar.entity.MPVacature;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VacatureMatchesRequestGemeente {
    @NotBlank
    private String aanvraagKenmerk;

    @NotNull
    @Valid
    private MPVacature vraagObject;

    @Override
    public String toString() {
        return "VacatureMatchesRequestGemeente{" +
                "aanvraagKenmerk='" + aanvraagKenmerk + '\'' +
                ", vraagObject=" + vraagObject +
                '}';
    }
}
