package nl.vng.vacatures_bemiddelaar.exception;

public class VraagIdAlreadyInDatabaseException extends Exception {
    public VraagIdAlreadyInDatabaseException() {
        super("VraagID is already in database.");
    }
}
