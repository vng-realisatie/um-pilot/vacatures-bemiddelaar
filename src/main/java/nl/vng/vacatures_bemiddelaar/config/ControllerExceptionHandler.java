package nl.vng.vacatures_bemiddelaar.config;

import nl.vng.vacatures_bemiddelaar.exception.NoMoreAccessRequestsLeftException;
import nl.vng.vacatures_bemiddelaar.exception.UnprocessableException;
import nl.vng.vacatures_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.vacatures_bemiddelaar.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.vacatures_bemiddelaar.exception.MismatchCertWithOinVumProviderException;
import nl.vng.vacatures_bemiddelaar.util.Error;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String FOUT_BIJ_UITVOEREN_VAN_ZOEKVRAAG = "Fout bij uitvoeren van zoekvraag";
    public static final ImmutablePair<String, String> ONGELDIGE_AANROEP = ImmutablePair.of("400.01", "Ongeldige aanroep");
    public static final ImmutablePair<String, String> ONVOLLEDIGE_AANROEP = ImmutablePair.of("400.02", "Onvolledige aanroep");

    public static final ImmutablePair<String, String> TOO_MANY_REQUESTS = ImmutablePair.of("429.01", "Too Many Requests");
    public static final ImmutablePair<String, String> NIET_GEAUTORISEERDE_AANROEP = ImmutablePair.of("403.01", "Geen authorizatie");
    public static final ImmutablePair<String, String> FOUT_BIJ_UITVOEREN = ImmutablePair.of("422.01", FOUT_BIJ_UITVOEREN_VAN_ZOEKVRAAG);

    @ResponseBody
    @ExceptionHandler(NoMoreAccessRequestsLeftException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleNoMoreAccessRequestsLeftException(final NoMoreAccessRequestsLeftException exception) {
        return new Error(TOO_MANY_REQUESTS.getLeft(), TOO_MANY_REQUESTS.getRight(), exception.getLocalizedMessage());    }


    @ResponseBody
    @ExceptionHandler(MissingRequestHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleMissingRequestHeaderException(final MissingRequestHeaderException exception) {
        return new Error(ONVOLLEDIGE_AANROEP.getLeft(), ONVOLLEDIGE_AANROEP.getRight(), exception.getLocalizedMessage());
    }

    @ResponseBody
    @ExceptionHandler(MismatchRequestWithOinBemiddelaarException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Error handleMismatchRequestWithOinBemiddelaarException(final MismatchRequestWithOinBemiddelaarException exception) {
        return new Error(NIET_GEAUTORISEERDE_AANROEP.getLeft(), NIET_GEAUTORISEERDE_AANROEP.getRight(), exception.getLocalizedMessage());
    }
    
    @ResponseBody
    @ExceptionHandler(MismatchCertWithOinVumProviderException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Error handleMismatchCertWithOinVumProviderException(final MismatchCertWithOinVumProviderException exception) {
        return new Error(NIET_GEAUTORISEERDE_AANROEP.getLeft(), NIET_GEAUTORISEERDE_AANROEP.getRight(), exception.getLocalizedMessage());
    }

    @ResponseBody
    @ExceptionHandler(VraagIdNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleVraagIdNotFoundException(final VraagIdNotFoundException exception) {
        return new Error(ONGELDIGE_AANROEP.getLeft(), FOUT_BIJ_UITVOEREN_VAN_ZOEKVRAAG, exception.getLocalizedMessage());
    }

    @ResponseBody
    @ExceptionHandler(UnprocessableException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public Error handleUnprocessableException(final UnprocessableException exception) {
        return new Error(FOUT_BIJ_UITVOEREN.getLeft(), FOUT_BIJ_UITVOEREN.getRight(), exception.getLocalizedMessage()); //TODO: confirm code
    }

    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleConstraintViolationException(final ConstraintViolationException exception) {
        return new Error(ONGELDIGE_AANROEP.getLeft(), ONGELDIGE_AANROEP.getRight(), exception.getLocalizedMessage());
    }


    /**
     * Handles exceptions whenever an entity is not valid.
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers,
                                                                  final HttpStatus status, final WebRequest request) {

        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        Error response = new Error(ONGELDIGE_AANROEP.getLeft(), ONGELDIGE_AANROEP.getRight(), errors.toString());

        return super.handleExceptionInternal(ex, response, headers, status, request);
    }

    /**
     * Handles exceptions whenever incoming JSON is not able to be read.
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            final HttpMessageNotReadableException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {

        Error response = new Error(ONVOLLEDIGE_AANROEP.getLeft(), ONVOLLEDIGE_AANROEP.getRight(), ex.getLocalizedMessage());

        return super.handleExceptionInternal(ex, response, headers, status, request);
    }
}
