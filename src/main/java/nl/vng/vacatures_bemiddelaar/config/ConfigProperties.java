package nl.vng.vacatures_bemiddelaar.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Class that obtains environment variables from application.properties starting with prefix cap.
 */
@Configuration
@ConfigurationProperties(prefix = "vng")
@Getter
@Setter
public class ConfigProperties {
    private String callbackUrl;
    private String vumProviderOin;
    private String vumUrlMatches;
    private String vumUrlVumId;
    private String bemiddelaarProviderOin;
    private Integer daysToExpiry;
    private Integer maxTimesAccessible;
    private String[] setAllowedOrigins;
    private String[] setAllowedMethods;
    private String[] setAllowedHeaders;
}
