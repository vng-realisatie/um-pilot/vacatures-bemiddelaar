package nl.vng.vacatures_bemiddelaar.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

import java.util.Objects;

@Configuration
@SpringBootApplication(exclude = ElasticsearchDataAutoConfiguration.class)
public class ElasticConfig extends AbstractElasticsearchConfiguration {

    public static final String ELASTICSEARCH_URL = "elasticsearch.url";
    private final Environment evn;

    @Autowired
    public ElasticConfig(final Environment environment) {
        this.evn = environment;
    }

    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {

        ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo(Objects.requireNonNull(this.evn.getProperty(ELASTICSEARCH_URL)))
                .build();

        return RestClients.create(clientConfiguration).rest();
    }
}
