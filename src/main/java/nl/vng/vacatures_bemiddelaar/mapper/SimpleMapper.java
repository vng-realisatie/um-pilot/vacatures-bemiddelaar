package nl.vng.vacatures_bemiddelaar.mapper;


import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequestGemeente;
import org.mapstruct.Mapper;

/**
 * Interface from which Mapstruct generates a mapper between classes.
 */
@Mapper(
        componentModel = "spring"
)
public interface SimpleMapper {

    VacatureMatchesRequest vacatureMatchesRequestGemeenteToVacatureMatchesRequest(VacatureMatchesRequestGemeente vacatureMatchesRequestGemeente);
}
