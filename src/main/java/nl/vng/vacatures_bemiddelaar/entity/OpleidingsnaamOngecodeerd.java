package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;

/**
 * OpleidingsnaamOngecodeerd
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OpleidingsnaamOngecodeerd extends MPOpleidingsnaamOngecodeerd implements Opleidingsnaam {

    @Valid
    private OpleidingsnaamOngecodeerdImpl opleidingsnaamOngecodeerd;

    @Override
    public String toString() {
        return "OpleidingsnaamOngecodeerd{" +
                "opleidingsnaamOngecodeerd=" + opleidingsnaamOngecodeerd +
                '}';
    }
}
