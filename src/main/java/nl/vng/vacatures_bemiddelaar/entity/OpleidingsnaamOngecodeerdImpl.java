package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class OpleidingsnaamOngecodeerdImpl {

    @Size(max = 120)
    private String naamOpleidingOngecodeerd;

    @Size(max = 2000)
    private String omschrijvingOpleiding;

    @Override
    public String toString() {
        return "OpleindingsnaamOngecodeerdImpl{" +
                "naamOpleidingOngecodeerd='" + naamOpleidingOngecodeerd + '\'' +
                ", omschrijvingOpleiding='" + omschrijvingOpleiding + '\'' +
                '}';
    }
}
