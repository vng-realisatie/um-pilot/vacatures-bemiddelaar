package nl.vng.vacatures_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.time.LocalDate;

/**
 * MPArbeidsVoorwaarden
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@JsonInclude(Include.NON_NULL)
@Schema(description = "De voorwaarden waaronder werk wordt verricht.")
public class MPArbeidsVoorwaarden {
    @Valid
    @JsonFormat(pattern="yyyy-MM-dd")
    @Schema(description = "De datum van de eerste dag van de werkzaamheden.")
    private LocalDate datumAanvangWerkzaamheden;

    @Valid
    @JsonFormat(pattern="yyyy-MM-dd")
    @Schema(description = "De datum van de laatste dag waarop het adres gerelateerd is aan de PERSOON of de ONDERNEMING/INSTELLING.")
    private LocalDate datumEindeWerkzaamheden;

    @Size(max = 100)
    @Schema(description = "De omschrijving die een indicatie van de hoogte van het salaris aangeeft.")
    private String salarisIndicatie;

    @Override
    public String toString() {
        return "MPArbeidsVoorwaarden{" +
                "datumAanvangWerkzaamheden=" + datumAanvangWerkzaamheden +
                ", datumEindeWerkzaamheden=" + datumEindeWerkzaamheden +
                ", salarisIndicatie='" + salarisIndicatie + '\'' +
                '}';
    }
}
