package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;

/**
 * StraatadresBuitenland
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StraatadresBuitenland implements AdresDetailsBuitenland {

    @Valid
    private StraatadresBuitenlandImpl straatadresbuitenland;

    @Override
    public String toString() {
        return "StraatadresBuitenland{" +
                "straatadresbuitenland=" + straatadresbuitenland +
                '}';
    }
}
