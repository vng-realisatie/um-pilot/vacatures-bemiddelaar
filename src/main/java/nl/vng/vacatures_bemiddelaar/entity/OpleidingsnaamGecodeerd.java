package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.Valid;

/**
 * OpleidingsnaamGecodeerd
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OpleidingsnaamGecodeerd extends MPOpleidingsnaam implements Opleidingsnaam {

   @Valid
   private OpleidingsnaamGecodeerdImpl opleidingsnaamGecodeerd;

   @Override
   public String toString() {
      return "OpleidingsnaamGecodeerd{" +
              "opleidingsnaamGecodeerd=" + opleidingsnaamGecodeerd +
              '}';
   }
}
