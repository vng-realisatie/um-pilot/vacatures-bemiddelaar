package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.Valid;

/**
 * MPOpleidingsnaamOngecodeerd
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MPOpleidingsnaamOngecodeerd extends MPOpleidingsnaam {
	
    @Valid
    private MPOpleidingsnaamOngecodeerdImpl mpOpleidingsnaamOngecodeerd;
    
    @Override
    public String toString() {
        return "MPOpleidingsnaamOngecodeerd{" +
                "mpOpleidingsnaamOngecodeerd='" + mpOpleidingsnaamOngecodeerd + '\'' +
                '}';
    }
}
