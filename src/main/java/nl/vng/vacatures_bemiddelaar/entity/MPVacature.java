package nl.vng.vacatures_bemiddelaar.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;
/**
 * MPVacature
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MPVacature {

    @Size(max = 1)
    @JsonInclude(Include.NON_EMPTY)
    private String codeWerkEnDenkniveauMinimaal;

    @Min(1)
    @Max(2)
    @JsonInclude(Include.NON_EMPTY)
    private Integer indicatieLdrRegistratie;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    @JsonInclude(Include.NON_NULL)
    private LocalDate sluitingsDatumVacature;

    @Valid
    private List<MPSollicitatiewijze> sollicitatiewijze;

    @Valid
    private MPWerkgever werkgever;

    @Valid
    private SectorBeroepsEnBedrijfsleven sector;

    @Valid
    private MPArbeidsVoorwaarden arbeidsVoorwaarden;

    @Valid
    private List<Contractvorm> contractvorm;

    @Valid
    private Beroepsnaam beroep;

    @Valid
    private List<Werkervaring> werkervaring;

    @Valid
    private List<Rijbewijs> rijbewijs;

    @Valid
    private List<MPVervoermiddel> vervoermiddel;

    @Valid
    private Flexibiliteit flexibiliteit;

    @Valid
    private Werktijden werktijden;

    @Valid
    private List<Cursus> cursus;

    @Valid
    private List<MPOpleiding> opleiding;

    @Valid
    private List<Gedragscompetentie> gedragscompetentie;

    @Valid
    private List<Vakvaardigheid> vakvaardigheid;

    @Valid
    private List<Taalbeheersing> taalbeheersing;

    @Override
    public String toString() {
        return "MPVacature{" +
                "codeWerkEnDenkniveauMinimaal='" + codeWerkEnDenkniveauMinimaal + '\'' +
                ", indicatieLdrRegistratie=" + indicatieLdrRegistratie +
                ", sluitingsDatumVacature=" + sluitingsDatumVacature +
                ", sollicitatiewijze=" + sollicitatiewijze +
                ", werkgever=" + werkgever +
                ", sector=" + sector +
                ", arbeidsVoorwaarden=" + arbeidsVoorwaarden +
                ", contractvorm=" + contractvorm +
                ", beroep=" + beroep +
                ", werkervaring=" + werkervaring +
                ", rijbewijs=" + rijbewijs +
                ", vervoermiddel=" + vervoermiddel +
                ", flexibiliteit=" + flexibiliteit +
                ", werktijden=" + werktijden +
                ", cursus=" + cursus +
                ", opleiding=" + opleiding +
                ", gedragscompetentie=" + gedragscompetentie +
                ", vakvaardigheid=" + vakvaardigheid +
                ", taalbeheersing=" + taalbeheersing +
                '}';
    }
}
