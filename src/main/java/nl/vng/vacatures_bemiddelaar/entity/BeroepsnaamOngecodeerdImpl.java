package nl.vng.vacatures_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class BeroepsnaamOngecodeerdImpl {

    @Size(max = 120)
    @Schema(description = "De omschrijving van het BEROEP.")
    private String naamBeroepOngecodeerd;

    @Override
    public String toString() {
        return "BeroepsnaamOngecodeerdImpl{" +
                "naamBeroepOngecodeerd='" + naamBeroepOngecodeerd + '\'' +
                '}';
    }
}
