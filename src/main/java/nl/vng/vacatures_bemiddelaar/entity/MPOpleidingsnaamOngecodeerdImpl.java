package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class MPOpleidingsnaamOngecodeerdImpl {

    @Size(max = 120)
    private String naamOpleidingOngecodeerd;

    @Override
    public String toString() {
        return "MPOpleidingsnaamOngecodeerdImpl{" +
                "naamOpleidingOngecodeerd='" + naamOpleidingOngecodeerd + '\'' +
                '}';
    }
}
