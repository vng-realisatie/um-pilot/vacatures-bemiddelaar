package nl.vng.vacatures_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class OpleidingsnaamGecodeerdImpl {
    @Size(max = 10)
    @Pattern(regexp = "^[0-9]+$")
    @Schema(description = "De unieke code van een OPLEIDINGSNAAM.")
    private String codeOpleidingsnaam;

    @Size(max = 120)
    @Schema(description = "De naam van de OPLEIDING.")
    private String omschrijvingOpleidingsnaam;

    @Override
    public String toString() {
        return "OpleidingsnaamGecodeerdImpl{" +
                "codeOpleidingsnaam='" + codeOpleidingsnaam + '\'' +
                ", omschrijvingOpleidingsnaam='" + omschrijvingOpleidingsnaam + '\'' +
                '}';
    }
}
