package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;

/**
 * MPAdresBuitenland
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class MPAdresBuitenland {
    @Valid
    @Embedded
    private MPAdresBuitenlandImpl adresBuitenland;

    @Override
    public String toString() {
        return "MPAdresBuitenland{" +
                "adresBuitenland=" + adresBuitenland +
                '}';
    }
}
