package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;

/**
 * PostbusadresBuitenland
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostbusadresBuitenland implements AdresDetailsBuitenland {

    @Valid
    private PostbusadresBuitenlandImpl postbusadresbuitenland;

    @Override
    public String toString() {
        return "PostbusadresBuitenland{" +
                "postbusadresbuitenland=" + postbusadresbuitenland +
                '}';
    }
}
