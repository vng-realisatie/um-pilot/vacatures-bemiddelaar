package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;

/**
 * AdresBuitenland
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdresBuitenland implements Adres {

    @Valid
    private AdresBuitenlandImpl adresBuitenland;

    @Override
    public String toString() {
        return "AdresBuitenland{" +
                "adresBuitenland=" + adresBuitenland +
                '}';
    }
}
