package nl.vng.vacatures_bemiddelaar.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class BeroepsnaamGecodeerdImpl {
    @Size(max = 10)
    @Schema(description = "De unieke code van de beroepsnaam uit het BO&C register.")
    private String codeBeroepsnaam;

    @Size(max = 120)
    @Schema(description = "De naam van het BEROEP.")
    private String omschrijvingBeroepsnaam;

    @Override
    public String toString() {
        return "BeroepsnaamGecodeerdImpl{" +
                "codeBeroepsnaam='" + codeBeroepsnaam + '\'' +
                ", omschrijvingBeroepsnaam='" + omschrijvingBeroepsnaam + '\'' +
                '}';
    }
}
