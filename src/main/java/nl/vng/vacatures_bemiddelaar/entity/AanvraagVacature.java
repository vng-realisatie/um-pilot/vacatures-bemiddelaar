package nl.vng.vacatures_bemiddelaar.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AanvraagVacature {

    @Size(max = 100)
    @Id
    private String vraagId;


    @OneToMany( //TODO: Many to many requires less data, but has orphan removal problem.
            mappedBy = "aanvraagVacatures",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Transient
    private Set<MPVacatureMatch> vacatures = new HashSet<>();

    @JsonIgnore
    private String oin;
    
    @Column(columnDefinition = "TEXT")
    private String zoekparameters;

    @NotBlank
    private String aanvraagKenmerk;

    private int timesAccessLeft;

    private LocalDate creatieDatum = LocalDate.now();

    public AanvraagVacature(final String id, final String theOin, final String kenmerk, final String zoekparameters, final Integer timesLeft) {
        this.vraagId = id;
        this.oin = theOin;
        this.aanvraagKenmerk = kenmerk;
        this.timesAccessLeft = timesLeft;
        this.zoekparameters = zoekparameters;
    }

    public void addVacature(final MPVacatureMatch vacature) {
        vacatures.add(vacature);
        vacature.setAanvraagVacatures(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AanvraagVacature aanvraagVacature1 = (AanvraagVacature) o;
        return Objects.equals(vraagId, aanvraagVacature1.vraagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vraagId);
    }

    @Override
    public String toString() {
        return "AanvraagVacature{" +
                "vraagId='" + vraagId + '\'' +
                ", oin='" + oin + '\'' +
                ", aanvraagKenmerk='" + aanvraagKenmerk + '\'' +
                ", timesAccessLeft=" + timesAccessLeft +
                ", creatieDatum=" + creatieDatum +
                '}';
    }
}
