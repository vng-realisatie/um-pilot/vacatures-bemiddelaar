package nl.vng.vacatures_bemiddelaar.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;

/**
 * Straatadres
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Straatadres implements AdresDetailsNederland {

    @Valid
    private StraatadresImpl straatadres;

    @Override
    public String toString() {
        return "Straatadres{" +
                "straatadres=" + straatadres +
                '}';
    }
}
