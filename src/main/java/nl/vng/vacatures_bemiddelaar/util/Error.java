package nl.vng.vacatures_bemiddelaar.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Error
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Error {

    @NotNull
    @Size(max = 10)
    private String code;

    @NotNull
    @Size(max = 500)
    private String message;

    @Size(max = 2000)
    private String details;
}
