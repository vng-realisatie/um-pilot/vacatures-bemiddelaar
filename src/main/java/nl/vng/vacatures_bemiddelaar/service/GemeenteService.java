package nl.vng.vacatures_bemiddelaar.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import nl.vng.vacatures_bemiddelaar.config.ConfigProperties;
import nl.vng.vacatures_bemiddelaar.dto.InlineResponse200;
import nl.vng.vacatures_bemiddelaar.dto.InlineResponse2001;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequestGemeente;
import nl.vng.vacatures_bemiddelaar.entity.AanvraagVacature;
import nl.vng.vacatures_bemiddelaar.entity.MPVacatureMatch;
import nl.vng.vacatures_bemiddelaar.entity.Vacature;
import nl.vng.vacatures_bemiddelaar.exception.NoMoreAccessRequestsLeftException;
import nl.vng.vacatures_bemiddelaar.exception.UnprocessableException;
import nl.vng.vacatures_bemiddelaar.exception.VraagIdAlreadyInDatabaseException;
import nl.vng.vacatures_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.vacatures_bemiddelaar.mapper.SimpleMapper;
import nl.vng.vacatures_bemiddelaar.repository.AanvraagVacatureRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


@Service
@Slf4j
public class GemeenteService {

    public static final String REST_CLIENT_ERROR = "Rest client error";
    public static final String X_VUM_BERICHT_VERSIE = "X-VUM-berichtVersie";
    public static final String X_VUM_FROM_PARTY = "X-VUM-fromParty";
    public static final String X_VUM_VIA_PARTY = "X-VUM-viaParty";
    public static final String X_VUM_TO_PARTY = "X-VUM-toParty";
    public static final String X_VUM_SUWIPARTY = "X-VUM-SUWIparty";

    private final AanvraagVacatureRepository repository;
    private final RestTemplate restTemplate;
    private final SimpleMapper mapper;
    private final ConfigProperties properties;


    public GemeenteService(final AanvraagVacatureRepository aanvraagVacatureRepository,
                           final RestTemplate template,
                           final SimpleMapper simpleMapper,
                           final ConfigProperties configProperties) {
        this.repository = aanvraagVacatureRepository;
        this.restTemplate = template;
        this.mapper = simpleMapper;
        this.properties = configProperties;
    }

    public List<AanvraagVacature> findAll(final String oin) {
        return repository.findByOin(oin);
    }

    public AanvraagVacature findByVraagIdAndOin(final String vraagId, final String oinBemiddelaar) {
        AanvraagVacature aanvraagInDb = repository.findByVraagIdAndOin(vraagId, oinBemiddelaar);
        if (aanvraagInDb == null) {
            throw new VraagIdNotFoundException();
        } else {
            return aanvraagInDb;
        }
    }

    /**
     * Make a rest call to VUM with the aanvraagVacature. Subsequently save the response vraag id in the database.
     *
     * @param aanvraagVacature aanvraagVacature to match on
     * @param oinBemiddelaar identifier of vacatures-bemiddelaar for example the requesting municipality
     * @return the response received from VUM
     */
    public InlineResponse200 makeRequestMatchesVum(final VacatureMatchesRequestGemeente aanvraagVacature, final String oinBemiddelaar) {
        VacatureMatchesRequest request = mapper.vacatureMatchesRequestGemeenteToVacatureMatchesRequest(aanvraagVacature);
        // Add callbackURL to request.
        request.setCallbackURL(properties.getCallbackUrl());
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        requestHeaders.set(X_VUM_BERICHT_VERSIE, "1.0.0");
        requestHeaders.set(X_VUM_FROM_PARTY, oinBemiddelaar);
        if (!"".equals(properties.getBemiddelaarProviderOin())) {
        	requestHeaders.set(X_VUM_VIA_PARTY, properties.getBemiddelaarProviderOin());
        }
        requestHeaders.set(X_VUM_TO_PARTY, properties.getVumProviderOin());
        requestHeaders.set(X_VUM_SUWIPARTY, "true");

        HttpEntity<VacatureMatchesRequest> matchesRequestEntity = new HttpEntity<>(request, requestHeaders);
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        
        String zoekparameters = null;
        try {
        	zoekparameters = mapper.writeValueAsString(aanvraagVacature);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
        	log.error("Mapper error aanvraagVacature");
        	throw new UnprocessableException("Fout bij verwerken van de vraag", e);
        }

        InlineResponse200 response;
        try {
            response = restTemplate.postForObject(properties.getVumUrlMatches(), matchesRequestEntity, InlineResponse200.class);
            createAanvraagVacature(Objects.requireNonNull(response).getVraagID(), oinBemiddelaar, aanvraagVacature.getAanvraagKenmerk(), zoekparameters);
        } catch (final VraagIdAlreadyInDatabaseException e) {
            log.error("Vraag id already in database");
            throw new UnprocessableException("Vraag id bestaat al in database");
        } catch (final RestClientException e) {
            log.error("Rest client error in response from VUM after request for matches." + e.getMessage());
            throw new UnprocessableException(REST_CLIENT_ERROR);
        } catch (final NullPointerException e) {
            log.error("NullPointer response from VUM after request for matches." + e.getMessage(), e);
            throw new UnprocessableException(REST_CLIENT_ERROR);
        }
        return response;
    }

    /**
     * Saves a AanvraagVacature with given vraagID and OIN.
     *
     * @param vraagId vraag id to save in the DB
     * @param oinBemiddelaar identifier of vacatures-bemiddelaar for example the requesting municipality
     * @param aanvraagKenmerk
     * @param zoekparameters - de uitgezette vraag
     * @throws VraagIdAlreadyInDatabaseException - if VraagID is already in the DB.
     * 
     */
    private void createAanvraagVacature(final String vraagId, final String oinBemiddelaar, final String aanvraagKenmerk, final String zoekparameters) throws VraagIdAlreadyInDatabaseException {
        if (repository.existsById(vraagId)) {
            throw new VraagIdAlreadyInDatabaseException();
        }
        repository.save(new AanvraagVacature(vraagId, oinBemiddelaar, aanvraagKenmerk, zoekparameters, properties.getMaxTimesAccessible()));
    }

    /**
     * Make a rest call to VUM for a detail vacature.
     *
     * @param vraagId - identifier of the initial request
     * @param vumId   - identifier of the vacature
     * @param oinBemiddelaar identifier of vacatures-bemiddelaar for example the requesting municipality
     * @return the response received from VUM
     */
    public Vacature makeRequestDetailVacature(final String vraagId, final String vumId, final String oinBemiddelaar) {
        AanvraagVacature aanvraagInDb = findByVraagIdAndOin(vraagId, oinBemiddelaar);

        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.registerModule(new Jdk8Module());

        try {
            checkTimesAccessLeft(aanvraagInDb);
            
            java.net.http.HttpRequest.Builder builder = HttpRequest.newBuilder()
                    .uri(new URI(properties.getVumUrlVumId() + vumId))
                    .header("Accept", "application/json")
                    .header(X_VUM_BERICHT_VERSIE, "1.0.0")
                    .header(X_VUM_FROM_PARTY, oinBemiddelaar)
                    .header(X_VUM_TO_PARTY, properties.getVumProviderOin())
                    .header(X_VUM_SUWIPARTY, "true")
                    .GET();
            
            if (!"".equals(properties.getBemiddelaarProviderOin())) {
            	builder.header(X_VUM_VIA_PARTY, properties.getBemiddelaarProviderOin());
            }
            
            HttpRequest request = builder.build();

            HttpResponse<String> clientResponse = HttpClient.newBuilder()
                    .build()
                    .send(request, HttpResponse.BodyHandlers.ofString());

            decreaseTimesAccessLeft(aanvraagInDb, vumId);
            markFetched(aanvraagInDb, vumId);

            InlineResponse2001 inlineResponse = objectMapper.readValue(clientResponse.body(), InlineResponse2001.class);

            return inlineResponse.getVacature();
        } catch (final RestClientException | NullPointerException | IOException | InterruptedException |
                       URISyntaxException e) {
            log.error("Error in response from VUM after request for detailed vacature.");
            throw new UnprocessableException(REST_CLIENT_ERROR);
        }
    }

    // Utility method to check if AanvraagVacature still has request left
    private void checkTimesAccessLeft(final AanvraagVacature vraagIdInDb) {
        if (vraagIdInDb.getTimesAccessLeft() <= 0) {
            throw new NoMoreAccessRequestsLeftException();
        }
    }

    // Utility method to decrease requests left of AanvraagVacature
    private void decreaseTimesAccessLeft(final AanvraagVacature vraagIdInDb, final String vumId) {
        MPVacatureMatch vacature = vraagIdInDb.getVacatures()
                .stream()
                .filter(mpVacatureMatch -> mpVacatureMatch.getVumID().equals(vumId))
                .findAny()
                .orElse(null);

        if (vacature != null && vacature.isFetchedDetail()) {
            return;
        }
        checkTimesAccessLeft(vraagIdInDb);

        vraagIdInDb.setTimesAccessLeft(vraagIdInDb.getTimesAccessLeft() - 1);
        repository.save(vraagIdInDb);
    }

    // Utility method to mark a vacature as fetched
    private void markFetched(final AanvraagVacature vraagIdInDb, final String vumId) {
        vraagIdInDb.getVacatures().forEach(vacature -> {
            if (vacature.getVumID().equals(vumId)) {
                vacature.setFetchedDetail(true);
            }
        });
        repository.save(vraagIdInDb);
    }

    /**
     * Remove all expired aanvragen from the specified days in the application properties.
     */
    public void removeAllExpiredAanvraag() {
        LocalDate expirydate = LocalDate.now().minusDays(properties.getDaysToExpiry());
        List<AanvraagVacature> expiredAanvragen = repository.findAllByCreatieDatumBefore(expirydate);
        repository.deleteAll(expiredAanvragen);
    }
}

