package nl.vng.vacatures_bemiddelaar.service;

import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesCallbackRequest;
import nl.vng.vacatures_bemiddelaar.entity.AanvraagVacature;
import nl.vng.vacatures_bemiddelaar.entity.MPVacatureMatch;
import nl.vng.vacatures_bemiddelaar.exception.VraagIdNotFoundException;
import nl.vng.vacatures_bemiddelaar.repository.AanvraagVacatureRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VumService {

    private final AanvraagVacatureRepository repository;

    public VumService(final AanvraagVacatureRepository aanvraagVacatureRepository) {
        this.repository = aanvraagVacatureRepository;
    }

    /**
     * Handle callback. Either the AanvraagVacature is already in the DB or it still has to be added.
     *
     * @param callbackRequest
     */
    public void handleCallback(final VacatureMatchesCallbackRequest callbackRequest) {
        Optional<AanvraagVacature> aanvraagInDbOpt = repository.findById(callbackRequest.getVraagID());
        if (aanvraagInDbOpt.isEmpty()) {
            throw new VraagIdNotFoundException();
        } else {
            AanvraagVacature aanvraagInDb = aanvraagInDbOpt.get();
            for (MPVacatureMatch vacature : callbackRequest.getMatches().getMpVacatureMatches()) {
                aanvraagInDb.addVacature(vacature);
            }
            repository.save(aanvraagInDb);
        }
    }


}
