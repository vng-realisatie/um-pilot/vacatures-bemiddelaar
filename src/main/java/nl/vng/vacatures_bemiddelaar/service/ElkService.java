package nl.vng.vacatures_bemiddelaar.service;

import nl.vng.vacatures_bemiddelaar.dto.ElkEntity;
import nl.vng.vacatures_bemiddelaar.repository.RequestRepository;
import org.springframework.stereotype.Service;

@Service
public class ElkService {

    private final RequestRepository requestRepository;

    public ElkService(final RequestRepository repository) {
        this.requestRepository = repository;
    }

    /**
     * Save the match request in ElasticSearch
     *
     * @param request            Request to be saved
     * @param toOin              OIN of the recipient
     * @param fromOin            OIN of the sender
     * @param requestSignature   Request signature
     * @param requestDescription Request description
     * @param requestPath        Request path
     */
    public void handleRequest(
            final Object request,
            final String toOin,
            final String fromOin,
            final String requestSignature,
            final String requestDescription,
            final String requestPath
    ) {
        requestRepository.save(new ElkEntity(
                request,
                toOin,
                fromOin,
                requestSignature,
                requestDescription,
                requestPath
        ));
    }
}


