package nl.vng.vacatures_bemiddelaar.controller;

import nl.vng.vacatures_bemiddelaar.config.ConfigProperties;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesCallbackRequest;
import nl.vng.vacatures_bemiddelaar.exception.MismatchCertWithOinVumProviderException;
import nl.vng.vacatures_bemiddelaar.service.ElkService;
import nl.vng.vacatures_bemiddelaar.service.VumService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;


@RestController
@Validated
@Slf4j
@CrossOrigin()
public class VumController {
	
	public static final String X_VUM_BERICHT_VERSIE = "X-VUM-berichtVersie";
    public static final String X_VUM_FROM_PARTY = "X-VUM-fromParty";
    public static final String X_VUM_VIA_PARTY = "X-VUM-viaParty";
    public static final String X_VUM_TO_PARTY = "X-VUM-toParty";
    public static final String X_VUM_SUWIPARTY = "X-VUM-SUWIparty";
    public static final String X_FORWARDED_FOR = "X-Forwarded-For";

    private final VumService vumService;

    private final ElkService elkService;
    
    private final ConfigProperties config;

    public VumController(final VumService service, final ElkService elk, final ConfigProperties configProperties) {
        this.vumService = service;
        this.elkService = elk;
        this.config = configProperties;

    }
    
    /**
     * POST /callback : callback methode
     * Callback methode om matching vacature binnen te krijgen voor Vraag ID.
     *
     * @param xVUMBerichtVersie (required)
     * @param xVUMToParty       (required)
     * @param xVUMFromParty     (required)
     * @param xVUMVraagID       (required)
     * @param callbackRequest   (required)
     * @param xVUMViaParty      (optional)
     * @return OK (status code 200)
     * or Fout bij uitvoeren van zoekvraag (status code 400)
     * or not authorized (status code 401)
     */
    @ApiOperation(value = "callback methode", nickname = "callback", notes = "Callback methode om matching vacature binnen te krijgen voor Vraag ID.", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Fout bij uitvoeren van zoekvraag"),
            @ApiResponse(code = 401, message = "not authorized")})
    @PostMapping(
            value = "/aanvraagvacature/callback",
            consumes = {"application/json"}
    )
    public ResponseEntity<VacatureMatchesCallbackRequest> callback(final @ApiParam(value = "", required = true) @RequestHeader(value = X_VUM_BERICHT_VERSIE, required = true) String xVUMBerichtVersie,
                                                                   final @Pattern(regexp="^\\d{20}$") @ApiParam(value = "", required = true) @RequestHeader(value = X_VUM_TO_PARTY, required = true) String xVUMToParty,
                                                                   final @Pattern(regexp="^\\d{20}$") @ApiParam(value = "", required = true) @RequestHeader(value = X_VUM_FROM_PARTY, required = true) String xVUMFromParty,
                                                                   final @ApiParam(value = "", required = true) @RequestHeader(value = "X-VUM-vraagID", required = true) String xVUMVraagID,
                                                                   final @ApiParam(value = "", required = true) @RequestHeader(value = X_FORWARDED_FOR, required = true) String xForwardedFor,
                                                                   final @ApiParam(value = "", required = true) @Valid @RequestBody VacatureMatchesCallbackRequest callbackRequest,
                                                                   final @Pattern(regexp="^\\d{20}$") @ApiParam(value = "") @RequestHeader(value = X_VUM_VIA_PARTY, required = false) String xVUMViaParty) {
        log.info("Callback ontvangen voor toParty OIN: " + xVUMToParty + " voor vraagID: " + xVUMVraagID);
        log.info("Receiving Headers xVUMBerichtVersie={}, xVUMToParty={}, xVUMViaParty={}, xVUMFromParty={}, xVUMVraagID={}", xVUMBerichtVersie,xVUMToParty,xVUMViaParty, xVUMFromParty, xVUMVraagID);
        //log.info("Receiving vraagobject {}", callbackRequest);
        
        validateXForwardedForHeader(xForwardedFor);
        
        vumService.handleCallback(callbackRequest);
        log.info("Succesvolle afhandeling callback voor toParty OIN: " + xVUMToParty + " voor vraagID: " + xVUMVraagID);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set(X_VUM_BERICHT_VERSIE, "1.0.0");
        responseHeaders.set(X_VUM_FROM_PARTY, xVUMToParty);
        if (!"".equals(xVUMViaParty)) {            	
        	responseHeaders.set(X_VUM_VIA_PARTY, xVUMViaParty);
        }
        responseHeaders.set(X_VUM_TO_PARTY, xVUMFromParty);
        
        elkService.handleRequest(
                callbackRequest,
                xVUMToParty,
                xVUMFromParty,
                "POST /aanvraagvacature/callback",
                "Callback methode om matching vacature binnen te krijgen voor Vraag ID",
                "/aanvraagvacature/callback"
        );
        
        return new ResponseEntity<>(responseHeaders, HttpStatus.OK);

    }
    
    /**
     * Indien xForwardedFor geen VUM provider OIN gooi exceptie.
     * Gebruik de VUM provider OIN zonder voorloop nullen
     * 
     * @param xForwardedFor
     */
    private void validateXForwardedForHeader(String xForwardedFor) {
    	String vumProviderOinTrimmed = config.getVumProviderOin().replaceFirst("^0+(?!$)", "");
    	if (!xForwardedFor.contains(vumProviderOinTrimmed)) {
        	log.info(X_FORWARDED_FOR + "='"+ xForwardedFor + "'");
            throw new MismatchCertWithOinVumProviderException(); 
        }		
	}

}
