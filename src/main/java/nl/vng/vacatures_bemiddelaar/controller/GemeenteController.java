package nl.vng.vacatures_bemiddelaar.controller;

import nl.vng.vacatures_bemiddelaar.dto.InlineResponse200;
import nl.vng.vacatures_bemiddelaar.dto.VacatureMatchesRequestGemeente;
import nl.vng.vacatures_bemiddelaar.entity.AanvraagVacature;
import nl.vng.vacatures_bemiddelaar.entity.Vacature;
import nl.vng.vacatures_bemiddelaar.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.vacatures_bemiddelaar.service.ElkService;
import nl.vng.vacatures_bemiddelaar.service.GemeenteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@Slf4j
@CrossOrigin()
public class GemeenteController {

    private final GemeenteService service;

    private final ElkService elkService;

    @Autowired
    public GemeenteController(final GemeenteService gemeenteService, final ElkService elk) {
        this.service = gemeenteService;
        this.elkService = elk;
    }

    /**
     * GET /aanvraagvacature/lijst/{oin} : Endpoint to retrieve all outstanding aanvragen for the given OIN.
     *
     * @param oin identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Internal Server Error (status code 500)
     */
    @GetMapping("/aanvraagvacature/lijst/{oin}")
    public List<AanvraagVacature> allAanvraagVacatures(final @PathVariable String oin) {
        log.info("Verzoek voor alle uitstaande vacatures aanvragen van OIN: " + oin);
        if (getOinUser().equals(oin)) {
            List<AanvraagVacature> result = service.findAll(oin);
            log.info("Succesvolle verzoek voor alle uitstaande vacatures aanvragen van OIN: " + oin);
            elkService.handleRequest(
                    null,
                    oin,
                    getOinUser(),
                    "GET /aanvraagvacature/lijst/{oin}",
                    "Endpoint to retrieve all outstanding aanvragen for the given OIN",
                    "/aanvraagwerkzoekende/lijst/" + oin
            );
            return result;
        } else {
            log.info("Mislukte verzoek voor alle uitstaande vacatures door OIN: " + getOinUser() + "Er is geprobeerd te uploaden voor OIN:" + oin);
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }

    /**
     * GET /aanvraagvacature/{oin}/{vraagId} : Endpoint to retrieve aanvraagvacature for the given OIN with vraag ID.
     *
     * @param oin     identifier of municipality
     * @param vraagId identifier of aanvraagVacature.
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Unprocessable request (status code 422)
     * or Internal Server Error (status code 500)
     */
    @GetMapping("/aanvraagvacature/{oin}/{vraagId}")
    public AanvraagVacature getAanvraagVacature(final @PathVariable String oin, final @PathVariable String vraagId) {
        log.info("Verzoek voor vacature met vraagID: " + vraagId + " van OIN: " + oin);

        if (getOinUser().equals(oin)) {
            AanvraagVacature result = service.findByVraagIdAndOin(vraagId, oin);
            log.info("Succesvolle verzoek voor vacature met vraagID: " + vraagId + " van OIN: " + oin);
            elkService.handleRequest(
                    null,
                    oin,
                    getOinUser(),
                    "GET /aanvraagvacature/{oin}/{vraagId}",
                    "Endpoint to retrieve aanvraagvacature for the given OIN with vraag ID",
                    "/aanvraagvacature/" + oin + "/" + vraagId
            );

            return result;
        } else {
            log.info("Mislukte verzoek voor vacature met vraagID: " + vraagId + " van OIN: " + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    getOinUser(),
                    "GET /aanvraagvacature/{oin}/{vraagId}",
                    "Endpoint to retrieve aanvraagvacature for the given OIN with vraag ID",
                    "/aanvraagvacature/" + oin + "/" + vraagId
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }

    /**
     * GET /aanvraagvacature/{oin}/{vumId} : Endpoint to retrieve detailed vacature for given vumID.
     *
     * @param oin     identifier of municipality
     * @param vraagId identifier of the AanvraagVacature
     * @param vumId   identifier of vacature
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Unprocessable request (status code 422)
     * or Internal Server Error (status code 500)
     */
    @GetMapping("/aanvraagvacature/detail/{oin}/{vraagId}/{vumId}")
    public Vacature getDetailVacature(final @PathVariable String oin, final @PathVariable String vraagId, final @PathVariable String vumId) {
        log.info("Verzoek voor detail vacature met vraagID: " + vraagId + " vumID: " + vumId + " van OIN: " + oin);
        if (getOinUser().equals(oin)) {
            Vacature result = service.makeRequestDetailVacature(vraagId, vumId, oin);
            log.info("Succesvolle verzoek voor detail vacature met vraagID: " + vraagId + " vumID: " + vumId + " van OIN: " + oin);
            elkService.handleRequest(
                    result,
                    oin,
                    getOinUser(),
                    "GET /aanvraagvacature/detail/{oin}/{vraagId}/{vumId}",
                    "Endpoint to retrieve detailed vacature for given vumID",
                    "/aanvraagvacature/detail/" + oin + "/" + vraagId + "/" + vumId
            );
            return result;
        } else {
            log.info("Mislukte verzoek voor detail vacature met vraagID: " + vraagId + " vumID: " + vumId + " van OIN: " + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    getOinUser(),
                    "GET /aanvraagvacature/detail/{oin}/{vraagId}/{vumId}",
                    "Endpoint to retrieve detailed vacature for given vumID",
                    "/aanvraagvacature/detail/" + oin + "/" + vraagId + "/" + vumId
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }

    /**
     * POST /aanvraagvacature/{oin} : Endpoint to post a request for vacatures matching aanvraagVacature.
     *
     * @param aanvraagVacature AanvraagVacature to match against
     * @param oin              identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Unprocessable request (status code 422)
     * or Internal Server Error (status code 500)
     */
    @PostMapping("/aanvraagvacature/{oin}")
    public InlineResponse200 createAanvraagVacature(final @Valid @RequestBody VacatureMatchesRequestGemeente aanvraagVacature,
                                                    final @PathVariable String oin) {
        log.info("Verzoek voor vacature van OIN: " + oin);
        if (getOinUser().equals(oin)) {
            InlineResponse200 result = service.makeRequestMatchesVum(aanvraagVacature, oin);
            log.info("Succesvolle verzoek voor vacature van OIN: " + oin + " Verzoek heeft gekregen vraagID: " + result.getVraagID());
            log.info("Sending vraagObject {}", aanvraagVacature);
            elkService.handleRequest(
                    aanvraagVacature,
                    oin,
                    getOinUser(),
                    "POST /aanvraagvacature/{oin}",
                    "Endpoint to post a request for vacatures matching aanvraagVacature",
                    "/aanvraagvacature/" + oin
            );
            return result;
        } else {
            log.info("Mislukte verzoek voor vacature van OIN: " + getOinUser() + "Er is geprobeerd een verzoek in te dienen voor OIN:" + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    getOinUser(),
                    "POST /aanvraagvacature/{oin}",
                    "Endpoint to post a request for vacatures matching aanvraagVacature",
                    "/aanvraagvacature/" + oin
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }


    /**
     * Utility method to extract OIN from the jwt token given by the user.
     *
     * @return OIN of the user or empty string if no OIN given.
     */
    private String getOinUser() {
        Authentication authentication = (Authentication)
                SecurityContextHolder.getContext().getAuthentication();

        String oinUser = ((JwtAuthenticationToken) authentication).getToken().getClaim("oin");
        
        return oinUser == null ?  "" : oinUser;
    }
}

