FROM adoptopenjdk/openjdk11

ARG ARG_APP_VERSION=0.0.1-SNAPSHOT

ENV TZ=Europe/Amsterdam

EXPOSE 8080

COPY target/vacatures_bemiddelaar-${ARG_APP_VERSION}.jar vacatures-bemiddenlaar.jar

ENTRYPOINT ["java","-jar","/vacatures-bemiddenlaar.jar"]
